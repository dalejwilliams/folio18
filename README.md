The project is the folio of Dale J Williams, published in September 2018.

The project is built on-top of Typescript and React and makes use of its own state machine. Feel free to explore.

### Notable Folders:

 - src/folio: The display and UI code
 - src/model: The model and router
