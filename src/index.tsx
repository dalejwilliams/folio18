import * as React from 'react';
import * as ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import { Chrome } from './folio/Chrome';

ReactDOM.render(
	<Chrome />,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
