import * as React from 'react';
import styled from 'styled-components';

import { model } from '../model';
import { Styles } from './Styles';

interface ThumbProps {
	image: string;
	title: string;
	section: string;
}

interface ThumbState {
	selected: boolean
}

const ThumbHolder = styled.div`
	display: inline-grid;
	padding: 0.2em;
	width: 15.5%;
	padding: 0.5%;
	cursor: pointer;

	@media ${Styles.MediaMobile} {
		width: 31%;
		padding: 1%;
	}
`;
const ThumbImage = styled.div`
	background-color: #EEE;
	height: 5em;
	border-radius: 0.5em;
	border-top: 2px solid rgba(0,0,0,0.3);
	background-size: cover;
	background-position: center;
`;
const ThumbTitle = styled.div`
	margin-top: -0.5em;
	font-size: 0.8em;
	line-height: 1em;
	height: 1.8em;
	border-radius: 0.5em;
	font-weight: bold;
	padding: 0.3em;
	vertical-align: middle;
	border-top-left-radius: 0;
	border-top-right-radius: 0;
	border-top: 1px solid rgba(0,0,0,0.1);
	overflow: hidden;
`;
export class Thumb extends React.Component<ThumbProps, ThumbState> {

	state = {
		selected: false
	};

	componentDidMount() {
		model.onSection.subscribe(section => {
			this.setState({ selected: section.key == this.props.section });
		});
	}

	handleClick() {
		model.setSection(this.props.section);
	}

	render() {
		const thumbStyle: React.CSSProperties = {
			backgroundImage: `url('${this.props.image}')`
		};
		return (
			<ThumbHolder className={this.state.selected ? 'selected' : ''} onClick={() => this.handleClick()}>
				<ThumbImage style={thumbStyle} className="area-highlight"/>
				<ThumbTitle className="area-highlight">
					{this.props.title}
				</ThumbTitle>
			</ThumbHolder>
		)
	}

}
