import * as React from 'react';
import styled from 'styled-components';
import { model } from '../model';
import { Styles } from './Styles';

const RibbonCont = styled.div`
	position: fixed;
	top: 0;
	left: 5px;
	width: 200px;
	height: 300px;
	background: url('/images/ribbon.svg') no-repeat top center;
	background-size: contain;
	cursor: pointer;
	transition: height 0.2s ease-in-out;

	&.small {
		width: 100px;
		height: 120px;
	}

	@media ${Styles.MediaMobile} {
		width: 110px;
		height: 160px;

		&.small {
			width: 80px;
			height: 80px;
		}
	}

	@media (min-width: 1000px) {
		left: 30%;
		margin-left: -300px;
	}
`;

const LogoCont = styled.div`
	margin: -5% 0;
	width: 100%;
	height: 100%;
	background: url('images/logo.png') no-repeat center;
	background-size: contain;
`;

interface LogoState {
	isPlaying: boolean;
	isReading: boolean;
}

export class Logo extends React.Component {

	state: LogoState = {
		isPlaying: model.onMedia.currentValue,
		isReading: model.onReadingContent.currentValue
	};

	componentDidMount() {
		model.onMedia.subscribe(isPlaying => this.setState({ isPlaying, isReading: this.state.isReading }));
		model.onReadingContent.subscribe(isReading => this.setState({ isPlaying: this.state.isPlaying, isReading }));
	}

	private handleClick() {
		model.setSection('home');
	}
    
	render() {
		const isSmall = this.state.isPlaying || this.state.isReading;
		return (
			<RibbonCont 
				className={isSmall ? 'small' : ''} 
				onClick={() => this.handleClick()}
				>
				<LogoCont>
				</LogoCont>
			</RibbonCont>
		);
	}

}
