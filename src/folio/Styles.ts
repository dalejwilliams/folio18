export class Styles {
	static readonly MobileWidth = 450;
	static readonly MediaMobile = `(max-width: ${Styles.MobileWidth}px)`;
	static readonly ContentWidth = 900;
	static readonly MediaUnderContent = `(max-width: ${Styles.ContentWidth}px)`;
}
