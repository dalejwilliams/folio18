import * as React from 'react';
import styled from 'styled-components';
import { model, ISection } from '../model';
import { Styles } from './Styles';

const BodyContainer = styled.div`
	margin: -0.5em 0 0;
	line-height: 1.5em;
	padding: 40px 30px;
	box-shadow: 0 3px 0 black;

	@media ${Styles.MediaMobile} {
		padding: 30px 20px;
	}
`;

interface BodyState {
	section: ISection
}

export class Body extends React.Component<{}, BodyState> {

	state = {
		section: model.onSection.currentValue
	};

	componentDidMount() {
		model.onSection.subscribe(section => {
			this.setState({ section });
		});
	}

	render() {
		let title = '';
		let body:any = '';
		if (this.state.section) {
			title = this.state.section.title;
			if (this.state.section.render) {
				body = this.state.section.render();
			}
		}

		return (
			<BodyContainer className="area-body">
				<h3>{title}</h3>
				{body}
			</BodyContainer>
		)
	}

}
