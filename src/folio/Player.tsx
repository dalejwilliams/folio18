import * as React from 'react';
import styled from 'styled-components';

import { ContentType, ISection, model } from '../model';
import { VideoModule } from './player-modules/index';
import { Styles } from './Styles';

export interface PlayerState {
		section: ISection;
		height: number;
}

const PlayerCont = styled.div`
	height: 500px;
	background-color: #000;
`;

export class Player extends React.Component<{}, PlayerState> {
    
	state: PlayerState = {
		section: model.onSection.currentValue,
		height: 0
	};

	componentDidMount() {
		model.onSection.subscribe(section => {
			this.setState({ ...this.state, section });
		});
		model.onResize.subscribe(dimensions => {
			const width = Math.min(dimensions.width, Styles.ContentWidth);
			const height =  Math.floor(width / 16 * 9);
			this.setState({ ...this.state, height });
		});
	}

	render() {
		const content = this.state.section.content || [];
		let playerContent = <div />;
		if (this.state.section.contentType == ContentType.Video) {
			playerContent = <VideoModule src={content[0] || ''} loop={this.state.section.contentLoop == true}/>;
		}
		return (
			<PlayerCont style={{height: this.state.height + 'px'}}>
				{playerContent}
			</PlayerCont>
		)
	}

}
