import * as React from 'react';
import styled from 'styled-components';

import { Body } from './Body';
import { Footer } from './Footer';
import { Logo } from './Logo';
import { Styles } from './Styles';
import { ThumbBar } from './ThumbBar';
import { Player } from './Player';

const TopBarHeight = '50px';
const TopBarTransition = '0.2s ease-in-out';

const ChromeContainer = styled.div`
	max-width: ${Styles.ContentWidth}px;
	margin: 0 auto;
	padding-bottom: 30px;
	transition: margin-top ${TopBarTransition};

	@media ${Styles.MediaMobile} {
		margin-top: ${TopBarHeight};
	}
`;

const TopBar = styled.div`
	position: fixed;
	top: 0;
	left: 0;
	right: 0;
	height: 0;
	transition: height ${TopBarTransition};
	margin: 0 auto;
	box-shadow: 0 3px 0 rgba(0,0,0,0.2);

	@media ${Styles.MediaMobile} {
		height: ${TopBarHeight};
	}
`;

export class Chrome extends React.Component {

	render() {
		return (
			<div className="body-container">
				<ChromeContainer className="body-chrome">
					<Player />
					<ThumbBar />
					<Body />
					<Footer />
				</ChromeContainer>
				<TopBar className="area-container" />
				<Logo />
			</div>
		)
	}

}
