import * as React from 'react';
import { model } from '../../model';

interface VideoProps {
	src: string;
	loop?: boolean;
	onSize?: (target: VideoModule) => void;
}

interface VideoState {
	lastSrc: string;
}

export class VideoModule extends React.Component<VideoProps, VideoState> {

	state = {
		lastSrc: ''
	};
	videoRef = React.createRef<HTMLVideoElement>();

	private onCanPlay() {
		if (this.state.lastSrc !== this.props.src) {
			if (this.videoRef.current) {
				this.videoRef.current.play();
				this.setState({ lastSrc: this.props.src });
			}
		}
	}

	private setPlaying(value: boolean) {
		model.setMediaPlaying(value);
	}

	private onEnded() {
		this.setPlaying(false);
		const videoElem = this.videoRef.current;
		if (videoElem) {
			videoElem.currentTime = 0;
			if (this.props.loop !== true) {
				videoElem.pause();
			}
		}
	}

	componentWillUnmount() {
		this.setPlaying(false);
	}

	render() {
		return (
			<div>
				<video ref={this.videoRef}
					src={this.props.src} 
					controls 
					width="100%" 
					onCanPlay={() => this.onCanPlay()}
					onPlay={() => this.setPlaying(true)}
					onPause={() => this.setPlaying(false)}
					onEnded={() => this.onEnded() }
					/>
			</div>
		);
	}
}
