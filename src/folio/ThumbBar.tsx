import * as React from 'react';
import styled from 'styled-components';

import { model } from '../model/Model';
import { Thumb } from './Thumb';
import { Styles } from './Styles';

const ThumbsContainer = styled.div`
		text-align: center;
		box-shadow: 0 3px 0 rgba(0,0,0,0.3);
		margin: -0.5em 0 0;
		transform: rotate(1);

		@media ${Styles.MediaMobile} {
			transform: rotate(0);
		}
`;

export class ThumbBar extends React.Component {

	render() {
		const thumbs = [];
		for (const section of model.sections) {
			if (section.hide === true) {
				continue;
			}
			thumbs.push(
				<Thumb 
					title={section.shortTitle || section.title} 
					image={section.thumb} 
					section={section.key}
					key={section.key}
					/>
			);
		}
		return (
			<ThumbsContainer className="area-device slanted">
				{thumbs}
			</ThumbsContainer>
		)
	}
}
