import * as React from 'react';
import styled from 'styled-components';

const FooterContent = styled.div`
	text-align: center;
`;

export class Footer extends React.Component {
    
	render() {
		return (
			<FooterContent className="area-outer">
				<p>
					&copy; Dale J Williams 2018
					<br/>
					<small>
						<a href="https://www.linkedin.com/in/dalejwilliams/" target="_blank">LinkedIn</a> : <a href="https://soundcloud.com/dwillprevail" target="_blank">SoundCloud</a>
						<br/>
						To get in touch, email dale[at]dalejwilliams[dot]com
					</small>
				</p>
			</FooterContent>
		);
	}

}
