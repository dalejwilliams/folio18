import { ISection } from "./ISection";
import { 
	SectionHome,
	SectionHBCloud,
	SectionOceanAdventure,
	SectionPrototypes,
	SectionRoubler,
	SectionSail,
	SectionSwipeStuff
} from "./sections/index";
import { Signal } from "./Signal";

interface Dimensions {
	width: number;
	height: number;
}

class Model {
	sections: ISection[] = [
		new SectionHome(),
		new SectionRoubler(),
		new SectionHBCloud(),
		new SectionSwipeStuff(),
		new SectionOceanAdventure(),
		new SectionSail(),
		new SectionPrototypes()
	];
	defaultSection: ISection = this.sections[0];
	onSection = new Signal<ISection>(this.defaultSection);
	onMedia = new Signal<boolean>(false);
	onReadingContent = new Signal<boolean>(false);
	onResize = new Signal<Dimensions>(this.getDimensions());

	constructor() {
		if (window.location.hash) {
			let key = window.location.hash.substr(1);
			if (key.charAt(0) == '/') {
				key = key.substr(1);
			}
			const section = this.getSection(key) || this.defaultSection;
			this.setSection(section.key);
		}
		this.tick();
	}

	setSection(key: string): void {
		this.setMediaPlaying(false);
		let section = this.getSection(key);
		if (section == null) {
			section = this.defaultSection;
		}
		this.onSection.dispatch(section);
		window.location.hash = "/" + section.key;
	}

	setMediaPlaying(value: boolean) {
		this.onMedia.dispatchIfChanged(value);
	}

	getSection(key: string): ISection|null {
		const section = this.sections.find(section => this.keyCompare(section.key, key));
		return section || null;
	}

	keyCompare(a: string, b: string): boolean {
		return typeof a === 'string' && typeof b === 'string' && a.toUpperCase().trim() === b.toUpperCase().trim();
	}

	private tick() {
		const scroll = window.scrollY;
		const isNowReading = scroll > 200;
		this.onReadingContent.dispatchIfChanged(isNowReading);

		const dimensions = this.getDimensions();
		this.onResize.dispatchIfChanged(dimensions);

		window.requestAnimationFrame(() => this.tick());
	}

	private getDimensions(): Dimensions {
		return {
			width: window.innerWidth,
			height: window.innerHeight
		};
	}
}

export const model = new Model();
