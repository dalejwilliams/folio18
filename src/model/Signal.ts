export interface SignalListener<T> {
	method: (data: T) => void;
	once?: boolean;
}

export class Signal<T> {

	private listeners: SignalListener<T>[] = [];
	public currentValue: T;

	constructor(initialData: T) {
		this.currentValue = initialData;
	}

	add(method: (data: T) => void, once: boolean = false) {
		this.remove(method);
		this.listeners.push({
			method, 
			once: once === true
		});
	}

	subscribe(method: (data: T) => void, once: boolean = false) {
		this.add(method, once);
		method(this.currentValue);
	}

	remove(method: (data: T) => void) {
		const index = this.listeners.findIndex(listener => listener.method === method);
		if (index >= 0) {
			this.listeners.splice(index, 1);
		}
	}

	dispatch(data: T) {
		this.currentValue = data;
		for (const listener of this.listeners) {
			listener.method(data);
		}
		for (let i=this.listeners.length-1; i>=0; i--) {
			if (this.listeners[i].once === true) {
				this.listeners.slice(i, 1);
			}
		}
	}

	dispatchIfChanged(data: T) {
		if (typeof data === 'object' && typeof data !== null) {
			let isChanged = false;
			for (var key in data) {
				if (data[key] === this.currentValue[key]) {
					continue;
				}
				isChanged = true;
				break;
			}
			if (isChanged === false) {
				return;
			}
		}
		if (this.currentValue === data) {
			return;
		}
		this.dispatch(data);
	}
	
}
