import * as React from 'react';

import { ISection } from "../ISection";
import { ContentType } from "../ContentType";

export class SectionOceanAdventure implements ISection {
	key = "ocean";
	title = "Ocean Adventure";
	thumb = "images/thumb_ocean.png";
	contentType = ContentType.Video;
	content = ['/videos/ocean.webm']

	render() {
		return (
			<div>
				<p>Ocean Adventure was a thirty minute long adventure game demo I made in 2017.</p>

				<p>The main character is a kid who wants to be a pirate and your objective is
					to gain a ship big enough to exit the bay.
				</p>

				<p>The game was made in <b>Unity 3D</b>, programmed in <b>C#</b>, with InControl
				being the only external library, used to normalise gamepad inputs.</p>
				
				<p>I modelled and animated the character and environment 
				models in <b>Blender</b>, designed the UI in <b>Inkscape</b>.</p>
			</div>
		);
	}
}
