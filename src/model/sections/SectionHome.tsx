import * as React from 'react';

import { ISection } from "../ISection";
import { ContentType } from "../ContentType";

export class SectionHome implements ISection {
	key = "home";
	title = "Dale J Williams 2018";
	thumb = "home/thumb.png";
	contentType = ContentType.Video;
	hide = true;

	render() {
		return (
			<div>
				<h4>Welcome to my shortfolio!</h4>
				<p>Here you'll find some videos of my recent work.<br/>
				Click the thumbs above to begin viewing.</p>
				<p><i>Be advised I began compiling this folio on the 26th of September and am likely to provide better
					videos in short time.</i></p>
				<hr />
				<h3>About Me</h3>
				<p>I'm a cross-disciplined developer, specialising in front-end and games, though I have significant experience
					in server, API and database development.</p> 
				<p>For more information, consult my <a href="/Dale_J_Williams_Resume.pdf" target="_blank">Resumé</a>.</p>
				<hr />
				<h3>This Folio</h3>
				<p>Although I'm more experienced in Angular, this folio was written in React <i>for giggles</i>.</p>
				<p>This site is mobile-compatible.</p>
			</div>
		);
	}
}
