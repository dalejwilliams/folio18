import * as React from 'react';

import { ISection } from "../ISection";
import { ContentType } from "../ContentType";

export class SectionSwipeStuff implements ISection {
	key = "swipe";
	title = "Swipe Stuff";
	thumb = "images/thumb_swipe.png";
	contentType = ContentType.Video;
	content = ['/videos/swipe.webm']

	render() {
		return (
			<div>
				<p>Swipe Stuff is a mobile game I'm developing for release in 2019.</p>

				<p>In the game, you move a burglar by swiping your phone. Every time
					you move, the enemy moves. Once you've <i>swiped</i> the gem, an
					alarm is triggered and you must exit through the entrance.</p>

				<p>The game is being made in <b>Unity 3D</b>, programmed in <b>C#</b>, with
				no external libraries.</p>

				<p>The game makes heavy use of procedural generation including maze generation
					algorthims to give the levels shape. The systems makes use of dynamically scaling
					difficulty curves, prop placement, pathfinding and solvers to create interesting
					levels.</p>
				
				<p>I modelled and animated the characters and environment 
				props in <b>Blender</b>, designed the UI in <b>Inkscape</b> and <b>Affinity Designer</b>.</p>
			</div>
		);
	}
}
