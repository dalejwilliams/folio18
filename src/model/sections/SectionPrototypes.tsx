import * as React from 'react';

import { ISection } from "../ISection";
import { ContentType } from "../ContentType";

export class SectionPrototypes implements ISection {
	key = "prototypes";
	title = "Prototypes";
	thumb = "images/thumb_prototypes.png";
	contentType = ContentType.Video;


	render() {
		return (
			<div>
				<p><b>Unfortunately, I'm still compiling the video right now. Check back soon.</b></p>

				<p>The prototypes in this video are game ideas I've developed in my own time.</p>
			</div>
		);
	}
}
