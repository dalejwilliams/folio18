import * as React from 'react';

import { ISection } from "../ISection";
import { ContentType } from "../ContentType";

export class SectionSail implements ISection {
	key = "sail";
	title = "Sail of the 16th Century";
	shortTitle = "Sail Century";
	thumb = "images/thumb_sail.png";
	contentType = ContentType.Video;
	contentLocked = true;

	render() {
		return (
			<div>
				<p><b>Unfortunately, I'm still compiling the video right now. Check back soon.</b></p>
				<p>Sail of the 16th Century was a prototype I lead at Halfbrick.</p>
				<p>In the prototype, players drove a pirate ship, attempting to loot islands.</p>
				<p>The prototype went through several successful rounds and the team was grown to
					eleven people but was ultimately cancelled when financial milestones weren't met.</p>
				<p>The game contains a steering mechanic that I'm particularly proud of and makes use
					of procedural generation and component-based models to give the ships and islands their 
					appareance.</p>
			</div>
		);
	}
}
