import * as React from 'react';

import { ISection } from "../ISection";
import { ContentType } from "../ContentType";

export class SectionRoubler implements ISection {
	key = "roubler";
	title = "Roubler";
	thumb = "images/thumb_roubler.png";
	contentType = ContentType.Video;
	content = ["/videos/roubler.webm"];

	render() {
		return (
			<div>
				<p>Roubler aims to be an all-in-one workforce management solution. Including
					functions for hiring, rostering, managing and paying staff.</p> 
				
				<p>I began work there as a frontend developer in 2016, working closely with 
					the founder to give shape to the product vision in <b>AngularJS.</b></p>

				<p>Once we'd created a working frontend on demo data, I moved roles
					to build a <b>NodeJS</b> and <b>PostgreSQL</b> backend on top of <b>AWS</b>, while continuing to
					build out the front-end.</p>

				<p>We launched the service to a pilot customer after just three months of development, 
					quickly iterating on features as we built up the team.</p>

				<p>Moving roles to the Head of Software Development, I helped build and lead a team of six, along with
					the CTO, while maintaining the backend and databse, as we expanded to over five thousand customers.</p>

				<p>Over the course of 2018, we grew the product to launch to two further countries, 
					migratating a lot of the front-end to newer <b>Angular 6</b> modules, while maintaining
					comatability and interopability with the AngularJS modules.</p>

				<p>I built the majority of Roubler's front-end and back-end systems, with noteworthy innovations being
					the draggable rostering tool that proved to be very popular with clients, and a tablet application 
					in <b>Cordova</b> for signing staff in and out of their shifts: it included offline redundancy for 
					clients in low Wi-Fi signal scenarios.</p>
			</div>
		);
	}
}
