import * as React from 'react';

import { ISection } from "../ISection";
import { ContentType } from "../ContentType";

export class SectionHBCloud implements ISection {
	key = "hbcloud";
	title = "Halfbrick Cloud";
	thumb = "images/thumb_halfbrick.png";
	contentType = ContentType.Video;
	content = ['/videos/hbcloud.mp4']

	render() {
		return (
			<div>
				<p>Halfbrick serves player data, events, virtual purchases and challenges to millions of 
					active users every day through Fruit Ninja and Jetpack Joyride.</p>

				<p>During 2014 and 2015, I worked on the team responsible for helping maintaining these services.</p>

				<p>I was also the primary maintainer and authour of the <b>AngularJS</b> frontend that allowed game
				teams and community managers to maintain user data and publish live game changes to millions of players.</p>

				<p>I was also part of a small team that built the user-facing in-game account management / login software
					found in the majority of Halfbrick games. I was one of two frontend developers working in that role.
					The technology is a mix of <b>C++</b> and <b>Javascript</b>.</p>

				<p>The Halfbrick Cloud Services are in <b>NodeJS</b> built upon <b>AWS</b>.</p>
			</div>
		);
	}
}
