import { ContentType } from "./ContentType";

export interface ISection {
	key: string;
	title: string;
	shortTitle?: string;
	thumb: string;
	contentType: ContentType;
	content?: string[];
	hide?: boolean;
	contentLocked?: boolean;
	contentLoop?: boolean;
	render: () => JSX.Element;
}
